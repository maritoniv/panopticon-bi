(function() {
var toc =  [{"type":"item","name":"Creating Area charts","url":"Working_with_Area_charts.htm"},{"type":"item","name":"Creating Stacked Area charts","url":"Creating_Stacked_Area_charts.htm"}];
window.rh.model.publish(rh.consts('KEY_TEMP_DATA'), toc, { sync:true });
})();