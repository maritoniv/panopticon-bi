(function() {
var toc =  [{"type":"item","name":"Creating Pie charts","url":"Creating_Pie_charts.htm"},{"type":"item","name":"Creating Donut charts","url":"Creating_Donut_charts.htm"},{"type":"item","name":"Creating Funnel charts","url":"Creating_Funnel_charts.htm"},{"type":"item","name":"Creating Tree Map charts","url":"Creating_Tree_Map_charts.htm"}];
window.rh.model.publish(rh.consts('KEY_TEMP_DATA'), toc, { sync:true });
})();