(function() {
var toc =  [{"type":"item","name":"Creating Gauge charts","url":"Creating_Gauge_charts.htm"},{"type":"item","name":"Creating Half Gauge charts","url":"Creating_Half_Gauge_charts.htm"},{"type":"item","name":"Creating Progress charts","url":"Creating_Progress_charts.htm"},{"type":"item","name":"Creating Arc charts","url":"Creating_Arc_charts.htm"}];
window.rh.model.publish(rh.consts('KEY_TEMP_DATA'), toc, { sync:true });
})();