(function() {
var toc =  [{"type":"item","name":"Creating a New Report Page","url":"Creating_a_New_Page.htm"},{"type":"item","name":"Creating and Defining Categories for Report Pages","url":"Defining_Categories_for_Report_Pages.htm"},{"type":"item","name":"Copying and Moving Pages","url":"Copying_and_Moving_Pages.htm"},{"type":"item","name":"Deleting Pages","url":"Deleting_Pages.htm"}];
window.rh.model.publish(rh.consts('KEY_TEMP_DATA'), toc, { sync:true });
})();